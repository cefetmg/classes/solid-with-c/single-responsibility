#ifndef _SERVICE_SORT_PERSON_SERVICE_
#define _SERVICE_SORT_PERSON_SERVICE_

#include <stdio.h>

void sort_from_input_to_output(FILE *const in, FILE *const out, int amount);

#endif