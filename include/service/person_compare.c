#include "service/person_compare.h"
#include <string.h>

int compare_persons(Person *const s, Person *const t) {
    return strcmp(s->name, t->name);
}