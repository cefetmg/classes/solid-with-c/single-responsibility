
#ifndef SERVICE_PERSON_COMPARE_H
#define SERVICE_PERSON_COMPARE_H

#include <stdio.h>
#include <stdlib.h>
#include "model/person.h"

int compare_persons(Person *const, Person *const);

#endif