#include "service/person_reader.h"

Person * next_person(FILE *const in) {
    char name[64];
    char profession[64];
    int age;

    if(fscanf(in, "%s %s %d", name, profession, &age) != 3) {
        return NULL;
    }

    return new_person((const char*) name, (const char*) profession, age);
}