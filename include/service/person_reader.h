
#ifndef PERSON_READER_H
#define PERSON_READER_H

#include <stdio.h>
#include <stdlib.h>
#include "model/person.h"

Person * next_person(FILE* const);

#endif