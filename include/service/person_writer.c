#include "service/person_writer.h"

void person_fprintf(Person *const person, FILE *const out) {
    char person_str[256];
    person_sprintf(person_str, person);
    fprintf(out, "%s\n", person_str);
}

void persons_fprintf(int const amount, Person **persons, FILE *const out) {
    int i;
    for(i = 0; i < amount; i++) {
        person_fprintf(persons[i], out);
    }
}