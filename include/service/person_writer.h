#ifndef SERVICE_PERSON_WRITER_H
#define SERVICE_PERSON_WRITER_H

#include <stdio.h>
#include <stdlib.h>
#include "model/person.h"

void person_fprintf(Person *const, FILE *const);

void persons_fprintf(int const, Person **const, FILE *const);

#endif