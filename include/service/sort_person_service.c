#include "sort_person_service.h"
#include "model/person.h"
#include "person_writer.h"
#include "person_reader.h"
#include "person_compare.h"
#define MIN(a, b) (a < b ? a : b)

int polymorphic_person_compare(void const *, void const *);

void sort_from_input_to_output(FILE *const in, FILE *const out, int amount) {
    Person *persons[amount];
    int i;
    for (i = 0; i < amount; i++) {
        persons[i] = next_person(stdin);

        if (persons[i] == NULL) {
            break;
        }
    }
    amount = MIN(amount, i);

    qsort(persons, amount, sizeof(Person *), &polymorphic_person_compare);

    fprintf(stdout, "name,address,age\n");
    persons_fprintf(amount, persons, stdout);
    
    for (i = 0; i < amount; i++) {
        delete_person(&persons[i]);
    }
}

int polymorphic_person_compare(void const *s, void const *t) {
    Person *cast_s = *(Person **) s;
    Person *cast_t = *(Person **) t;
    return compare_persons(cast_s, cast_t);
}