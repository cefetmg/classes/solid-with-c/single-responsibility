#include "person.h"
#include <stdlib.h>
#include <assert.h>
#include <string.h>

Person * new_person(const char *restrict name, const char *restrict profession, int age) {
    Person *person = malloc(sizeof(Person));

    person->name[0] = '\0';
    person->profession[0] = '\0';
    if (name != NULL) {
        strcpy(person->name, name);
    }

    if (profession != NULL) {
        strcpy(person->profession, profession);
    }

    person->age = age;
    
    return person;

}

void delete_person(Person **person) {
    free((*person));
}

int person_sprintf(char *output, Person *const person) {
    if (person == NULL) {
        return sprintf(output, "(null)");
    }
    
    return sprintf(output, "%s,%s,%d", person->name, person->profession, person->age);
}