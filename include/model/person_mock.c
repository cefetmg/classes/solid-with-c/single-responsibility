#include "person_mock.h"
#include <assert.h>
#include <string.h>

static PersonToStringMock *_person_sprintf_mock;

void _flush_person_fprintf_mock() {
    _person_sprintf_mock->calls = 0;
    _person_sprintf_mock->return_val[0] = '\0';
}

void _init_person_mocks() {
    _person_sprintf_mock = malloc(sizeof(PersonToStringMock));
    _flush_person_fprintf_mock();
}

void _destroy_person_mocks() {
    _flush_person_fprintf_mock();
    free(_person_sprintf_mock);
    _person_sprintf_mock = NULL;
}

void _set_person_fprintf_return(char *const return_val) {
    assert(return_val);
    strcpy(_person_sprintf_mock->return_val, return_val);
}

PersonToStringMock _get_person_fprintf_mock() {
    return *_person_sprintf_mock;
}

int person_sprintf(char *output, Person *const person) {
    fprintf(stderr, "person_mock.c: calling MOCK person_sprintf\n");
    _person_sprintf_mock->calls++;
    return  sprintf(output, "%s", _person_sprintf_mock->return_val);
}
