#ifndef PERSON_MOCK_H
#define PERSON_MOCK_H

#include "person.h"

typedef struct person_to_string_mock {
    int calls;
    char return_val[128];
} PersonToStringMock;

void _init_person_mocks();

void _destroy_person_mocks();

void _set_person_fprintf_return(char *const);

PersonToStringMock _get_person_fprintf_mock();

#endif
