#ifndef MODEL_PERSON_H
#define MODEL_PERSON_H

#include <stdio.h>
#include <stdlib.h>

typedef struct person {
    char name[64];
    char profession[64];
    int age;

} Person;

Person * new_person(const char *restrict, const char *restrict, const int);

void delete_person(Person **);

int person_sprintf(char *, Person *const);

#endif
