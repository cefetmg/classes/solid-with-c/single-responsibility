cmake_minimum_required(VERSION 3.10)

project("Single-Responsibility with C"
        VERSION 0.1.0
        DESCRIPTION "Getting to learn SOLID with C"
        LANGUAGES C)

add_library(person_model STATIC include/model/person.c include/model/person.h)
target_include_directories(person_model PUBLIC include)

add_library(person_model_mock STATIC include/model/person_mock.c
                                     include/model/person_mock.h
                                     include/model/person.h)
target_include_directories(person_model_mock PUBLIC include)

add_library(person_compare STATIC include/service/person_compare.c
                                  include/service/person_compare.h)
target_include_directories(person_compare PUBLIC include)

add_library(person_reader STATIC include/service/person_reader.c
                                 include/service/person_reader.h)
target_include_directories(person_reader PUBLIC include)

add_library(person_writer STATIC include/service/person_writer.c
                                 include/service/person_writer.h)
target_include_directories(person_writer PUBLIC include)

add_library(person_sorter STATIC include/service/sort_person_service.c
                                 include/service/sort_person_service.h)
target_include_directories(person_sorter PUBLIC include)

add_executable(person_main src/main.c)

target_link_libraries(person_main
                      PUBLIC 
                             person_sorter
                             person_model
                             person_compare
                             person_reader
                             person_writer)

# Enable testing
enable_testing()
include (CTest)

link_directories(test)

add_executable(person_model_test test/model/person_test.c)
target_include_directories(person_model_test PUBLIC include test)
target_link_libraries(person_model_test PUBLIC person_model)

add_test(NAME unit_test_person_model COMMAND person_model_test)

add_executable(person_writer_test test/service/person_writer_test.c)
target_include_directories(person_writer_test PUBLIC include test)
target_link_libraries(person_writer_test PUBLIC person_model_mock person_writer)

add_test(NAME unit_test_person_writer COMMAND person_writer_test)

# Integration testing
configure_file(test/integration/input ${CMAKE_CURRENT_BINARY_DIR}/input
               COPYONLY)

add_library(integration_test_utils STATIC test/integration/test_utils.c
                                          test/integration/test_utils.h)
target_include_directories(integration_test_utils PUBLIC test include)

add_executable(integration_lib_sort_service_test
               test/integration/person_sort_service_test.c)
target_include_directories(integration_lib_sort_service_test
                           PUBLIC include test)
target_link_libraries(integration_lib_sort_service_test
                      PUBLIC integration_test_utils
                             person_model
                             person_writer
                             person_reader
                             person_compare
                             person_sorter)

add_test(NAME integration_lib_sort_service_test
         COMMAND integration_lib_sort_service_test)
