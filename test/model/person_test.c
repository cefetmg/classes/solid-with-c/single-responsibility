#include "model/person.h"
#include <assert.h>
#include <string.h>
#include <stdio.h>

int should_create_person_with_all_args() {
    fprintf(stderr, "RUNNING should_create_person_with_all_args\n");
    Person *person = new_person("Rodrigo Novaes", "Software Engineer", 26);

    assert(person->age == 26);
    assert(!strcmp(person->name, "Rodrigo Novaes"));
    assert(!strcmp(person->profession, "Software Engineer"));
    delete_person(&person);
    fprintf(stderr, "RUNNING should_create_person_with_all_args: OK\n");
    return 0;
}

int should_print_person_correctly() {
    fprintf(stderr, "RUNNING should_print_person_correctly\n");
    Person *person = new_person("Luffy", "Capitain", 19);
    char person_str[64];

    person_sprintf(person_str, person);

    assert(!strcmp(person_str, "Luffy,Capitain,19"));
    delete_person(&person);
    fprintf(stderr, "RUNNING should_print_person_correctly: OK\n");
    return 0;
}

int should_print_person_without_name_correctly() {
    fprintf(stderr, "RUNNING should_print_person_without_name_correctly\n");
    Person *person = new_person(NULL, "Capitain", 19);
    char person_str[64];

    person_sprintf(person_str, person);
    
    assert(!strcmp(person_str, ",Capitain,19"));
    delete_person(&person);
    fprintf(stderr, "RUNNING should_print_person_without_name_correctly: OK\n");
    return 0;
}

int should_print_person_without_profession_correctly() {
    fprintf(stderr, "RUNNING should_print_person_without_profession_correctly\n");
    Person *person = new_person("Luffy", NULL, 19);
    char person_str[64];

    person_sprintf(person_str, person);
    
    assert(!strcmp(person_str, "Luffy,,19"));
    delete_person(&person);
    fprintf(stderr, "RUNNING should_print_person_without_profession_correctly: OK\n");
    return 0;
}

int should_print_person_without_both_name_and_profession_correctly() {
    fprintf(stderr, "RUNNING should_print_person_without_both_name_and_profession_correctly\n");
    Person *person = new_person(NULL, NULL, 19);
    char person_str[64];

    person_sprintf(person_str, person);

    assert(!strcmp(person_str, ",,19"));
    delete_person(&person);
    fprintf(stderr, "RUNNING should_print_person_without_both_name_and_profession_correctly: OK\n");
    return 0;
}

int main(int argc, char* argv[]) {
    return should_create_person_with_all_args() 
        | should_print_person_correctly()
        | should_print_person_without_name_correctly()
        | should_print_person_without_profession_correctly()
        | should_print_person_without_both_name_and_profession_correctly();
}
