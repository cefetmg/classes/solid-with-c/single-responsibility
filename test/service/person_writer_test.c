#include "service/person_writer.h"
#include "model/person.h"
#include "model/person_mock.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>

int should_call_person_sprintf_three_times() {
    fprintf(stderr, "RUNNING should_call_person_sprintf_three_times\n");
    _init_person_mocks();
    const int nOfPersons = 3;
    Person **listOfPersons = malloc(sizeof(Person *) * nOfPersons);
    _set_person_fprintf_return("Rodrigo Novaes");
    
    persons_fprintf(nOfPersons, listOfPersons, stdout);

    PersonToStringMock mockStats = _get_person_fprintf_mock();
    assert(mockStats.calls == 3);
    assert(!strcmp(mockStats.return_val, "Rodrigo Novaes"));
    _destroy_person_mocks();
    free(listOfPersons);
    fprintf(stderr, "RUNNING should_call_person_sprintf_three_times: OK\n");
    return 0;
}

int should_not_call_person_sprintf_mock() {
    fprintf(stderr, "RUNNING should_not_call_person_sprintf_mock\n");
    _init_person_mocks();
    const int nOfPersons = 0;
    Person** listOfPersons = malloc(sizeof(Person *) * nOfPersons);

    persons_fprintf(nOfPersons, listOfPersons, stdout);

    PersonToStringMock mockStats = _get_person_fprintf_mock();
    assert(mockStats.calls == 0);
    _destroy_person_mocks();
    free(listOfPersons);
    fprintf(stderr, "RUNNING should_not_call_person_sprintf_mock: OK\n");
    return 0;
}

int main(int argc, char* argv[]) {
    return should_call_person_sprintf_three_times()
     | should_not_call_person_sprintf_mock();
}