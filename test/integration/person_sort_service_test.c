#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "service/sort_person_service.h"
#include "service/person_compare.h"
#include "service/person_writer.h"
#include "service/person_reader.h"
#include "model/person.h"
#include "integration/test_utils.h"

int polymorphic_name_compare(const void *s, const void *t);

int should_order_output_by_name() {
    set_up("input");
    assert(input_file_stream() != NULL);
    int amount = 0;
    
    fscanf(input_file_stream(), "%d", &amount);
    Person *persons[amount];
    int i;
    for (i = 0; i < amount; i++) {
        persons[i] = next_person(input_file_stream());
    }

    qsort(persons, amount, sizeof(Person *), &polymorphic_name_compare);
    fprintf(stdout, "name,profession,age\n");
    persons_fprintf(amount, persons, stdout);

    assert(amount == 7);
    tear_down();
    for (i = 0; i < amount; i++) {
        delete_person(&persons[i]);
    }
    return 0;
}

int main() {
    return should_order_output_by_name();
}

int polymorphic_name_compare(const void *s, const void *t) {
    Person *cast_s = cast_from_void(s);
    Person *cast_t = cast_from_void(t);
    return compare_persons(cast_s, cast_t);
}