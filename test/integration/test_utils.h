#ifndef TEST_UTILS_H
#define TEST_UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include "model/person.h"

void set_up(const char *);

void tear_down();

FILE * input_file_stream();

Person * cast_from_void(const void *person);

#endif