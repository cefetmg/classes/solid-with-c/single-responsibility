#include "integration/test_utils.h"

FILE *_input_file;

void set_up(const char *filename) {
    _input_file = fopen(filename, "r");
}

void tear_down() {
    fclose(_input_file);
}

FILE * input_file_stream() {
    return _input_file;
}

Person * cast_from_void(const void *person) {
    return *((Person**) person);
}