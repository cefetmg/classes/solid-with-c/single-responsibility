#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "service/sort_person_service.h"

int main(void) {
    int amount;
    assert(scanf("%d", &amount) == 1);
    assert(amount > 0);

    sort_from_input_to_output(stdin, stdout, amount);

    return 0;
}